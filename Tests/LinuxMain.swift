import XCTest

import RPC_Client_SwiftTests

var tests = [XCTestCaseEntry]()
tests += RPC_Client_SwiftTests.allTests()
XCTMain(tests)
