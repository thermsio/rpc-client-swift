import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(RPC_Client_SwiftTests.allTests),
    ]
}
#endif
