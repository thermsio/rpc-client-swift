## RPC-Client-Swift

![rpc_logo.png](https://bitbucket.org/thermsio/rpc-server-ts/raw/a9f154178cbc69e9b821ed22fbe7bfec125cdf86/rpc_logo.png)

An easy-to-use client library specifically developed for iOS platform to interact with the server RPC framework.

- Node.js RPC server (https://bitbucket.org/thermsio/rpc-server-ts)
- JS/TS client (https://bitbucket.org/thermsio/rpc-client-ts)
- Android/Kotlin client (https://bitbucket.org/thermsio/rpc-client-kotlin)

## Features
[x] Caching Responses
[x] Duplicate In Flight Calls

## Requirements
- iOS 10.0+
- Xcode 11+
- Swift 5.1+

## Installation

### Swift Package Manager
The [Swift Package Manager](https://swift.org/package-manager/) is a tool for automating the distribution of Swift code and is integrated into the `swift` compiler. It is in early development, but Alamofire does support its use on supported platforms.

Once you have your Swift package set up, adding Alamofire as a dependency is as easy as adding it to the `dependencies` value of your `Package.swift`.

```swift
dependencies: [
    .package(url: "https://bitbucket.org/thermsio/rpc-client-swift", .upToNextMajor(from: "1.0.1"))
]
```
