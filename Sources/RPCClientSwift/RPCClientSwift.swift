import Foundation

public enum RPCCacheMode{
    case SWR //StaleWhileRevalidate
    case simpleCache //Default cache
    case noCache // No cache applied
}

public enum RPCSocketObserver : String{
    case connected = "RPCSocketConnectedObserver"
    case disconnected = "RPCSocketDisconnectedObserver"
}

open class RPCClient {
    
    // MARK: - Properties
    private var configuration: Configuration
    private let handler: APICallHandler
    private var identity: Identity?
    private var socketManager: SocketManager?
    // MARK: - Init Methods
    public init(config: Configuration) {
        self.configuration = config
        socketManager = SocketManager(url: URL(string: configuration.baseSocketUrl ?? ""), useInBackground: config.useWebsocketInBackground, appHeaders: config.httpHeaders, delegate: configuration.delegate)
        self.handler = APICallHandler(
            socketManager: socketManager!,
            httpManager: HTTPManager(url: URL(string: configuration.baseHttpUrl), appHeaders: config.httpHeaders),
            configuration: config
        )
        NetworkMonitor.shared.startMonitoring()
    }
    
    /// Will perform request and return the result in Any format
    public func performAnyRequest(for procedure: String, args: [String: Any], scope: String, version: String, cacheMode:RPCCacheMode = .noCache, forceHttpRequest:Bool = false, httpTimeout:TimeInterval? = nil, completion: ((_ request:RPCRequest, Result<Any, Error>) -> Void)?) {
        let request = RPCRequest(procedure: procedure, args: args, scope: scope, version: version, identity: identity)
        self.handler.performAny(request: request, configuration: configuration, cacheMode: cacheMode, forceHttpRequest: forceHttpRequest, httpTimeout: httpTimeout, completion: completion)
    }
    
    /// Will perform request and return the result in Data format
    public func performDataRequest(for procedure: String, args: [String: Any], scope: String, version: String, completion: ((Result<Data, Error>) -> Void)?) {
        let request = RPCRequest(procedure: procedure, args: args, scope: scope, version: version, identity: identity)
        self.handler.performData(request: request, configuration: configuration, completion: completion)
    }
    
    /// Will perform request and decode the result in given class/struct
    public func performDecodableRequest<T: Decodable>(for procedure: String, args: [String: Any], scope: String, version: String, completion: ((Result<T, Error>) -> Void)?) {
        let request = RPCRequest(procedure: procedure, args: args, scope: scope, version: version, identity: identity)
        self.handler.performDecodable(request: request, configuration: configuration, completion: completion)
    }
    
    public func sendClientMessageToServer(payload: Any){
        let request = ClientMessageRequest(payload: payload, identity: identity)
        self.handler.performClientMessageToServer(request: request)
    }
    
    ///Used to update identity
    public func updateIdentity(_ identity: Identity) {
        if(identity.authorization != ""){
            self.identity = identity
            self.clearIdentityCache()
        }else{
            self.identity = nil
            self.disconnectSocket()
        }
    }
    
    public func disconnectSocket(){
        if socketManager?.isConnected == true {
            socketManager?.disconnectSocket()
        }
    }
    
    ///Used to enable & disable caching for all API calls
    public func changeCachingStatus(enable: Bool) {
        self.configuration.enableCache = enable
        updateHandlerConfig()
    }
    
    ///Used to update the age of response caching
    public func changeCachingAge(ageInminutes: Int) {
        self.configuration.cacheAgeInMin = ageInminutes
        updateHandlerConfig()
    }
    
    ///Used to enable & disable duplicate in-flight calls.
    public func changeDuplicateInFlightCallStatus(useDuplicateInFlightCalls: Bool) {
        self.configuration.useDuplicateInFlightCall = useDuplicateInFlightCalls
        updateHandlerConfig()
    }
    
    ///To clear all cached responses.
    public func clearCache() {
        self.handler.clearCache()
    }
    
    public func getIdentity() -> Identity? {
        return self.identity
    }
    
    ///To clear identity cached.
    public func clearIdentityCache() {
        self.handler.clearIdentityCache()
    }
    
    private func updateHandlerConfig() {
        self.handler.update(configuration: configuration)
    }
 
    /// Will hold API related configuration
    public struct Configuration {
        
        public var enableCache: Bool = false
        public var cacheAgeInMin: Int = 0
        public let baseHttpUrl: String
        public var useDuplicateInFlightCall: Bool = false
        public let baseSocketUrl: String?
        public var useWebsocketInBackground: Bool = false
        public var delegate: RPCEventsDelegate?
        public var httpHeaders: [String:String]?
        public var enableDiskCache : Bool = true
        
        
        public init(enableCache: Bool = false, cacheAgeInMin: Int = 0, baseHttpUrl: String, useDuplicateInFlightCall: Bool = false, baseSocketUrl: String?,useWebsocketInBackground: Bool = false, delegate:RPCEventsDelegate? = nil, httpHeaders: [String:String]? = [:], enableDiskCache : Bool = true) {
            self.enableCache = enableCache
            self.cacheAgeInMin = cacheAgeInMin
            self.baseHttpUrl = baseHttpUrl
            self.useDuplicateInFlightCall = useDuplicateInFlightCall
            self.baseSocketUrl = baseSocketUrl
            self.useWebsocketInBackground = useWebsocketInBackground
            self.delegate = delegate
            self.httpHeaders = httpHeaders
            self.enableDiskCache = enableDiskCache
        }
        
    }
    
}
