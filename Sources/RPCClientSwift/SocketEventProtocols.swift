//
//  File.swift
//  
//
//  Created by Muhammad Usman on 28/06/2022.
//

import Foundation

public protocol RPCEventsDelegate {

    func onWsConnected()
    func onWsDisconnected()
    func onServerMessage(msj:[String:Any])
}
