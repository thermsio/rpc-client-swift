import Foundation

internal class Response {
    
    var cachedTime: Date
    var response: Any
    
    init(response: Any) {
        self.response = response
        self.cachedTime = Date()
    }
    
    func toDictionary()->StringAny{
        var params : StringAny = [:]
//        let dateStr = cach
        params["cachedTime"] = cachedTime.getDateString(with: timestampFormateString)
        params["response"] = response
        return params
    }
    
    init?(dictionary: [String: Any]) {
        guard let cachedTime = dictionary["cachedTime"] as? String,
              let response = dictionary["response"]
                // Add any other properties as needed
        else {
            return nil
        }
        self.cachedTime = Date().getDate(timestampFormateString, cachedTime) ?? Date()
        self.response = response
        // Initialize other properties if needed
    }
}
