//
//  File.swift
//  
//
//  Created by John Borisenko on 18/03/21.
//

import Foundation

public class Identity {
    
    public var authorization: String
    public var deviceName: String
    public var metadata: [String: Any]
    
    public init(authorization: String, deviceName: String, metadata: [String: Any]) {
        self.authorization = authorization
        self.deviceName = deviceName
        self.metadata = metadata
    }
    
    func isEqual(to identity: Identity) -> Bool {
        if authorization != identity.authorization,
           deviceName != identity.deviceName {
            return false
        }
        return true
    }
    
    public func getDictionary() -> [String: Any] {
        var dictionary = StringAny()
        dictionary[ParameterKey.authorization] = authorization
        dictionary[ParameterKey.deviceName] = deviceName
        dictionary[ParameterKey.metadata] = metadata
        return dictionary
    }
    
    // Initialize the object from a dictionary
    init?(dictionary: [String: Any]) {
        guard let authorization = dictionary[ParameterKey.authorization] as? String,
              let deviceName = dictionary[ParameterKey.deviceName] as? String,let metadata = dictionary[ParameterKey.metadata] as? StringAny
                // Add any other properties as needed
        else {
            return nil
        }
        self.authorization = authorization
        self.deviceName = deviceName
        self.metadata = metadata
        // Initialize other properties if needed
    }
    
}
