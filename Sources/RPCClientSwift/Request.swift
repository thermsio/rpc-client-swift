//
//  File.swift
//  
//
//  Created by John Borisenko on 17/03/21.
//

import Foundation

public class RPCRequest {
    
    public var procedure: String
    public var args: [String:Any]
    public var scope: String
    public var version: String
    public var identity: Identity?
    public var correlationId: String
    
    public init(procedure: String, args: [String:Any], scope: String, version: String, identity: Identity?,correlationId:String = UUID().uuidString) {
        self.procedure = procedure
        self.args = args
        self.scope = scope
        self.version = version
        self.identity = identity
        self.correlationId = correlationId
    }
    
    // Initialize the object from a dictionary
    init?(dictionary: [String: Any]) {
        guard let procedure = dictionary[ParameterKey.procedure] as? String,
              let args = dictionary[ParameterKey.args] as? StringAny, let scope = dictionary[ParameterKey.scope] as? String,
              let version = dictionary[ParameterKey.version] as? String,
              let identity = dictionary[ParameterKey.identity] as? StringAny,
              let correlationId = dictionary[ParameterKey.correlationID] as? String
                // Add any other properties as needed
        else {
            return nil
        }
        self.procedure = procedure
        self.args = args
        self.scope = scope
        self.version = version
        self.identity = Identity(dictionary: identity)
        self.correlationId = correlationId
        // Initialize other properties if needed
    }
    
    public func createBody() throws -> Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: getDictionary(), options: [.prettyPrinted])
            return jsonData
        } catch {
            throw error
        }
    }
    
    func createBody(excludeIdentity:Bool) throws -> Data {
        do {
            var params = getDictionary()
            if(excludeIdentity && hasIdentity()){
                params.removeValue(forKey: ParameterKey.identity)
            }
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: [.prettyPrinted])
            return jsonData
        } catch {
            throw error
        }
    }
    
    func hasIdentity()->Bool{
        let params = getDictionary()
        let keyExists = params[ParameterKey.identity] != nil
        return keyExists
    }
    
    func getCacheKey() -> String {
        var keyValue = ""
        var argsKeyValue = ""
        let argsKeys = args.keys.sorted()
        argsKeys.forEach { (key) in
            argsKeyValue += "\(key)=\(String(describing: args[key]!))"
        }
        let dictionary = getDictionaryForCache()
        let allKeys = dictionary.keys.sorted()
        allKeys.forEach { (key) in
            if key == ParameterKey.args {
                keyValue += "\(key)=\(argsKeyValue)"
            } else {
                keyValue += "\(key)=\(String(describing: dictionary[key]!))"
            }
        }
        return keyValue
    }
    
    public func getDictionary() -> [String:Any] {
        var allParams = StringAny()
        allParams[ParameterKey.procedure] = procedure
        allParams[ParameterKey.args] = args
        allParams[ParameterKey.scope] = scope
        allParams[ParameterKey.version] = version
        allParams[ParameterKey.correlationID] = correlationId
        if let identity = identity {
            allParams[ParameterKey.identity] = identity.getDictionary()
        }
        return allParams
    }
    
    func getDictionaryForCache() -> StringAny {
        var allParams = StringAny()
        allParams[ParameterKey.procedure] = procedure
        allParams[ParameterKey.args] = args
        allParams[ParameterKey.scope] = scope
        allParams[ParameterKey.version] = version
        if let identity = identity {
            allParams[ParameterKey.identity] = identity.getDictionary().sorted {$0.key < $1.key}
        }
        return allParams
    }
}

class RequestResponse {
    var request: RPCRequest
    var response: Response
    
    init(request: RPCRequest, response: Response) {
        self.request = request
        self.response = response
    }
    
    func isEqual(to request: RPCRequest) -> Bool {
        if self.request.scope != request.scope,
           self.request.version != request.version {
            return false
        }
        if let oldIdentity = request.identity, let newIdentity = request.identity, !newIdentity.isEqual(to: oldIdentity) {
            return false
        }
        return true
    }
    
    func getDictionary() -> StringAny {
        var dictionary = StringAny()
        dictionary["request"] = request.getDictionary()
        dictionary["response"] = response.toDictionary()
        return dictionary
    }
    
    // Initialize the object from a dictionary
    init?(dictionary: [String: Any]) {
        guard let request = dictionary["request"] as? StringAny,
              let response = dictionary["response"] as? StringAny
                // Add any other properties as needed
        else {
            return nil
        }
        self.request = RPCRequest(dictionary: request)!
        self.response = Response(dictionary: response)!
        // Initialize other properties if needed
    }
}
