import Foundation
import Alamofire

internal struct HTTPManager {
    
    // MARK: - Properties
    private let baseUrl: URL?
    private let appHeaders: [String:String]?
    // MARK: - Init Method
    init(url: URL?, appHeaders: [String:String]? = [:]) {
        self.baseUrl = url
        self.appHeaders = appHeaders
    }
    
    // MARK: - Methods
    func perform(request: RPCRequest, httpTimeout:TimeInterval? = nil, completion: ResultAnyCompletion?) {
        let reqCorrId = request.correlationId
        guard let url = baseUrl else {
            completion?(reqCorrId, .failure(NSError.urlNotFound))
            return
        }
        
        var headerFields : HTTPHeaders = [
            HTTPHeaderField.contentType : ContentType.json,
            HTTPHeaderField.acceptType : ContentType.json
        ]
        //Adding headers from main app
        for (key, value) in self.appHeaders ?? [:] {
            headerFields.add(name: key, value: value)
        }
        
        if !NetworkMonitor.shared.isConnected {
            completion?(reqCorrId, .failure(NSError(domain: "Network not connected.", code: -1007, userInfo: nil)))
            return
        }
        
        #if DEBUG
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: request.getDictionary(), options: .prettyPrinted)
            print("Http Json Request")
            print(String(data: jsonData, encoding: .utf8) ?? "")
        } catch {
            print(error.localizedDescription)
        }
        #endif
        
        AF.request(url, method: .post, parameters: request.getDictionary(), encoding: JSONEncoding.default, headers: headerFields, requestModifier: { $0.timeoutInterval = httpTimeout ?? 15.0 }).responseData { response in
            DispatchQueue.main.async {
                switch response.result {
                case .success(let data):
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any> {
                            let resCorrId = NetworkUtils.getCorrelationId(response: jsonArray) ?? reqCorrId
                            completion?(resCorrId, .success(jsonArray))
                        }
                    } catch let error as NSError {
                        completion?(reqCorrId, .failure(error))
                    }
                case .failure(let afError):
                    if let error = afError.underlyingError {
                        completion?(reqCorrId, .failure(error))
                    } else {
                        completion?(reqCorrId, .failure(NSError(domain: afError.failureReason ?? "", code: afError.responseCode ?? 0, userInfo: [:])))
                    }
                }
            }
        }
    }
}
