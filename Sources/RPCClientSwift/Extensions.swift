//
//  File.swift
//  
//
//  Created by John Borisenko on 17/03/21.
//

import Foundation
import UIKit

let timestampFormateString = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

internal typealias VoidCompletion = (() -> Void)
internal typealias StringAny = [String: Any]
internal typealias ResultAnyCompletion = ((_ resCorrId:String?, Result<Any, Error>) -> Void)
internal typealias ResultDataCompletion = ((Result<Data, Error>) -> Void)
internal typealias SocketAnyCompletion = ((_ resCorrId:String?, Result<Any, Error>, _ requiredHttpRequest:Bool?) -> Void)

internal extension Dictionary {
    
    var jsonStringData: Data? {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted]) else {
            return nil
        }
        return jsonData
    }
}

internal extension NSError {
    
    static var urlNotFound: Error { NSError(domain: "URL_NOT_AVAILABLE", code: 404, userInfo: [:]) as Error }
    static var bodyCreation: Error { NSError(domain: "CANNOT_CREATE_BODY", code: 404, userInfo: [:]) as Error }
}

extension SocketManager{
    func setObserversForApplicationState(){
        // Removing observers if already added
        self.removeObserversForApplicationState()
        
        // initialise app state observers
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(SocketManager.appInBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(SocketManager.appInForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func removeObserversForApplicationState(){
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func addNetworkMonitorObserver(){
        
        self.removeNetworkMonitorObserver()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(showOfflineDeviceUI(notification:)), name: NSNotification.Name.connectivityStatus, object: nil)
    }
    
    func removeNetworkMonitorObserver(){
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.removeObserver(self, name: NSNotification.Name.connectivityStatus, object: nil)
    }
}

extension Date {
    func getDateString(with formate:String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        dateFormatter.timeZone = TimeZone.current//TimeZone(abbreviation: "UTC")
        return dateFormatter.string(from: self)
    }
    
    func getDate(_ formate:String, _ dateStr:String)->Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        dateFormatter.timeZone = TimeZone.current//TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: dateStr)
    }
}
