import Foundation

//private var cacheManager = NSCache<NSString, RequestResponse>()

internal class APICallHandler: NSObject {
    
    // MARK: - Properties
    private var configuration: RPCClient.Configuration
    private var socketManager: SocketManager
    private var httpManager: HTTPManager
    private let dispatchQueue = DispatchQueue(label: "rpc_client_swift_api_queue", qos: .background)
    private let semaphore = DispatchSemaphore(value: 0)
    
    private var cacheManager : CacheManager
    
    // requestion collections to get original request from correlationid
    private var requestColl : [String : RPCRequest] = [:]
    
    // MARK: - Init Methods
    init(socketManager: SocketManager, httpManager: HTTPManager, configuration: RPCClient.Configuration) {
        self.socketManager = socketManager
        self.httpManager = httpManager
        self.configuration = configuration
        
        cacheManager = CacheManager(configuration: configuration)
        
        //cache validity for all objects
        if configuration.enableDiskCache {
            cacheManager.checkCacheValidity()
        }
    }
    
    func update(configuration: RPCClient.Configuration) {
        self.configuration = configuration
        cacheManager.update(configuration: configuration)
        self.clearCache()
    }
    
    func clearCache() {
        cacheManager.clearCache()
    }
    
    func clearIdentityCache() {
        socketManager.clearIdentityCache()
    }
    
    // MARK: - Methods
    func performAny(request: RPCRequest, configuration: RPCClient.Configuration, cacheMode:RPCCacheMode = .noCache, forceHttpRequest:Bool = false, httpTimeout:TimeInterval? = nil, completion: ((_ request:RPCRequest, Result<Any, Error>) -> Void)?) {
        execute(request: request, configuration: configuration, cacheMode: cacheMode, forceHttpRequest: forceHttpRequest, httpTimeout: httpTimeout, completion: completion)
    }
    
    func performData(request: RPCRequest, configuration: RPCClient.Configuration, completion: ResultDataCompletion?) {
        execute(request: request, configuration: configuration) { request, result in
            switch result {
            case .success(let response):
                do {
                    let data = try JSONSerialization.data(withJSONObject: response, options: JSONSerialization.WritingOptions.prettyPrinted)
                    completion?(.success(data))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
        
    }
    
    func performDecodable<T: Decodable>(request: RPCRequest, configuration: RPCClient.Configuration, completion: ((Result<T, Error>) -> Void)?) {
        performData(request: request, configuration: configuration) { result in
            switch result {
            case .success(let responseData):
                do {
                    let decodedObj = try JSONDecoder().decode(T.self, from: responseData)
                    completion?(.success(decodedObj))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    // MARK: - Helper Methods
    fileprivate func execute(request: RPCRequest, configuration: RPCClient.Configuration, cacheMode:RPCCacheMode = .noCache, forceHttpRequest:Bool=false, httpTimeout:TimeInterval? = nil, completion: ((_ request:RPCRequest, Result<Any, Error>) -> Void)?) {
        
        self.requestColl[request.correlationId] = request
        
        if (configuration.enableCache && (cacheMode == .simpleCache || cacheMode == .SWR)), let requestResponse = cacheManager.retrieveDataFromMemoryCache(forKey: request.getCacheKey()) {
            
            //check for specific procedure cache
            if isCacheValid(dateTime: requestResponse.response.cachedTime) {
                self.requestColl.removeValue(forKey: request.correlationId)
                completion?(request, .success(requestResponse.response.response))
                //if cache has simple type should return from here if SWR should return cached value and proceed for the RPC call
                if(cacheMode == .simpleCache){
                    return
                }
            } else {
                cacheManager.removeCacheData(forKey: request.getCacheKey())
            }
        }
        
        if self.socketManager.isConnected, !forceHttpRequest {
//            dispatchQueue.async {
//
//                self.semaphore.wait()
//            }
            self.socketManager.perform(request: request) { (corrId, result,requiredHttpRequest) in
                var req = request
                if let corrId = corrId {
                    req = self.requestColl[corrId] ?? request
                }
                if requiredHttpRequest == true {
                    self.execute(request: req, configuration: configuration, cacheMode: cacheMode, forceHttpRequest: true, completion: completion)
                    return
                }
                self.requestColl.removeValue(forKey: req.correlationId)
                completion?(req, result)
                self.cache(request: request, result: result)
//                self.semaphore.signal()
            }
            
        } else {
            
            self.socketManager.reConnectSocket()
            
            self.httpManager.perform(request: request, httpTimeout: httpTimeout) { (corrId, result) in
                var req = request
                if let corrId = corrId {
                    req = self.requestColl[corrId] ?? request
                }
                self.requestColl.removeValue(forKey: req.correlationId)
                completion?(req, result)
                self.cache(request: request, result: result)
            }
        }
    }
    
    fileprivate func cache(request: RPCRequest, result: (Result<Any, Error>)) {
        if !configuration.enableCache {
            return
        }
        switch result {
        case .success(let res):
            let reqRes = RequestResponse(request: request, response: Response(response: res))
            cacheManager.saveDataToCache(data: reqRes, forKey: request.getCacheKey())
        case .failure(let error):
            print(error)
        }
    }
    
    fileprivate func isCacheValid(dateTime: Date) -> Bool {
        if let minutes = Calendar.current.dateComponents([.minute], from: dateTime, to: Date()).minute, minutes > configuration.cacheAgeInMin {
            return false
        }
        return true
    }
    
    
    //Client Message to server
    func performClientMessageToServer(request: ClientMessageRequest) {
        
        if self.socketManager.isConnected {
            self.socketManager.perform(request: request)
        }
        
    }
  
}

class CacheManager {

    private var cacheManager = NSCache<NSString, RequestResponse>()
    private var configuration: RPCClient.Configuration
    
    init(configuration: RPCClient.Configuration) {
        // Set up cache directory
        self.configuration = configuration
    }
    
    func update(configuration: RPCClient.Configuration) {
        self.configuration = configuration
    }
    
    // Function to save data to memory cache
    func saveDataToCache(data: RequestResponse, forKey key: String) {
        cacheManager.setObject(data, forKey: key as NSString)
        
        if self.configuration.enableDiskCache {
            let dictionary = data.getDictionary()
            CoreDataStack.shared.saveDataToCoreData(data: dictionary, apiKey: key)
            
        }
    }
    
    // Function to retrieve data from memory cache
    func retrieveDataFromMemoryCache(forKey key: String) -> RequestResponse? {
        if let retrievedData = cacheManager.object(forKey: key as NSString) {
            return retrievedData
        }else if self.configuration.enableDiskCache {

            if let dictionary = CoreDataStack.shared.fetchDataFromCoreData(apiKey: key),let reqResp = RequestResponse(dictionary: dictionary) {
                cacheManager.setObject(reqResp, forKey: key as NSString)
                return reqResp
            }
            
        }
        return nil
    }
    
    func checkCacheValidity(){
        if let dateTime = CoreDataStack.shared.fetchLastClearedDate() {
            if let minutes = Calendar.current.dateComponents([.minute], from: dateTime, to: Date()).minute, configuration.cacheAgeInMin > 0, minutes > configuration.cacheAgeInMin {
                self.clearCache()
                CoreDataStack.shared.updateLastClearedDate(date: Date())
            }
        }else {
            CoreDataStack.shared.updateLastClearedDate(date: Date())
        }
    }
    
    // Function to remove data from both memory and disk cache
    func removeCacheData(forKey key: String) {
        cacheManager.removeObject(forKey: key as NSString)
        if self.configuration.enableDiskCache {
            CoreDataStack.shared.removeCacheData(apiKey: key)
        }
    }
    
    // Function to clear all cached data
    func clearCache() {
        cacheManager.removeAllObjects()
        if self.configuration.enableDiskCache {
            CoreDataStack.shared.removeCacheData()
        }
    }
    
}
