//
//  File.swift
//  
//
//  Created by Muhammad Usman on 01/07/2021.
//

import Foundation

class NetworkUtils {
    class func checkIfSuccess(response:Dictionary<String, Any>)->Bool{
        if let networkStatus = response[ParameterKey.success] as? Bool {
            return networkStatus
        }
        return false
    }
    
    class func hasCorrelationId(response:Dictionary<String, Any>)->Bool{
        if let id = response[ParameterKey.correlationID] as? String {
            if(id != ""){
                return true
            }
        }
        return false
    }
    
    class func getCorrelationId(response:Dictionary<String, Any>)->String?{
        if let id = response[ParameterKey.correlationID] as? String {
            return id
        }
        return ""
    }
    
    class func hasServerMessage(response:Dictionary<String, Any>)->Bool{
        if let _ = response[ParameterKey.server_message]{
            return true
        }
        return false
    }
}
