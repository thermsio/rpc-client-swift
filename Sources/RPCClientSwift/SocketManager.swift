import Foundation
import Starscream

internal class SocketManager {
    
    // MARK: - Properties
    private var webSocket: WebSocket?
    private var currentStatus: WebSocketEvent = .cancelled
    private var useWebsocketInBackground: Bool = false
    private lazy var timestamp: Int64 = self.getCurrentMillis()
    private let SOCKET_AUTO_BACKGROUND_CLOSE_TIME:Int64 = 15000
    private let SOCKET_TIMEOUT_TIME : Double = 5.0
    private var autoCloseJob: Timer?
    private var autoConnectJob: Timer?
    private var lastAuthCorrelationId: String?
    private var isWsAuthenticated: Bool = false
    private var isAlreadyRequestedForConnect: Bool = false
    private var authenticationStatusChangeTimestamp:Int64 = 0
    var resultCompletion: [String : SocketAnyCompletion] = [:]
    public var delegate: RPCEventsDelegate?
    
    var isConnected: Bool {
        switch currentStatus {
        case .connected:
            return true
        default:
            return false
        }
    }
    
    // MARK: - Init Method
    init(url: URL?, useInBackground: Bool, appHeaders: [String:String]? = [:], delegate: RPCEventsDelegate?) {
        guard let url = url else {
            print("ERROR: cannot initialise url")
            return
        }
        var request = URLRequest(url: url)
        request.setValue(ContentType.json, forHTTPHeaderField: HTTPHeaderField.contentType)
        for (key, value) in appHeaders ?? [:] {
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        self.useWebsocketInBackground = useInBackground
        self.delegate = delegate
        webSocket = WebSocket(request: request)
        webSocket?.respondToPingWithPong = false
        webSocket?.delegate = self
        self.connectSocket()
        
        // observers to check app State (Foreground or background)
        self.setObserversForApplicationState()
        
        //set Observer for network Connectivity
        self.addNetworkMonitorObserver()
    }
       
    // MARK: - Methods
    func perform(request: RPCRequest, completion: @escaping SocketAnyCompletion) {
        do {
            print("requst##### \(request.correlationId)")
            print("requst identity##### \(String(describing: request.identity?.getDictionary()))")
            let identityStatus = checkForIdentityStatus(request: request)
            let paramData = try request.createBody(excludeIdentity: identityStatus)
            
            #if DEBUG
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: request.getDictionary(), options: .prettyPrinted)
                print("Socket Json Request")
                print(String(data: jsonData, encoding: .utf8) ?? "")
            } catch {
                print(error.localizedDescription)
            }
            #endif
            if !NetworkMonitor.shared.isConnected {
                completion(request.correlationId,.failure(NSError(domain: "Network not connected.", code: -1007, userInfo: nil)),false)
                return
            }
            
            let coreId = request.correlationId
            webSocket?.write(stringData: paramData, completion: nil)
            resultCompletion[coreId] = completion
            
            //check request timeout after 5 seconds
            DispatchQueue.main.async {
                Timer.scheduledTimer(timeInterval: self.SOCKET_TIMEOUT_TIME, target: self, selector: #selector(self.socketRequestTimeOutCall(_:)), userInfo: coreId, repeats: false)
            }
            
        } catch {
            completion(request.correlationId,.failure(NSError(domain: "Params Not Found", code: 404, userInfo: nil)),false)
        }
    }
    
    //Perform Client Message to server
    func perform(request: ClientMessageRequest) {
        do {
            print("requst identity##### \(String(describing: request.identity?.getDictionary()))")
            
            let identityStatus = checkClientMessageToServerIdentity(request: request)
            let payload = try request.createBody(excludeIdentity: identityStatus)
            
            //send data to server
            webSocket?.write(stringData: payload, completion: nil)
            
        } catch(let err) {
            print("Exception happened \(err.localizedDescription)")
        }
    }
    
    private func checkForIdentityStatus(request:RPCRequest)->Bool{
        if(request.hasIdentity()){
            if(getIsAlreadyAuthenticated()){
                return true
            }else{
                lastAuthCorrelationId = request.correlationId
                return false
            }
        }
        
        return false
    }
    
    private func checkClientMessageToServerIdentity(request:ClientMessageRequest)->Bool{
        if(request.hasIdentity()){
            
            if(getIsAlreadyAuthenticated()){
                return true
            }
        }
        
        return false
    }
    
    private func setIsAlreadyAuthenticated(value: Bool){
        isWsAuthenticated = value
        authenticationStatusChangeTimestamp = self.getCurrentMillis()
    }
    
    private func getIsAlreadyAuthenticated()-> Bool{
        if(isWsAuthenticated){
            if(authenticationStatusChangeTimestamp != 0 && self.getCurrentMillis() - authenticationStatusChangeTimestamp > 2000){
                return true
            }
        }
        return false
    }
    
    private func updateSocketAuthenticationStatus(response: Dictionary<String, Any>){
        let identityFromServer = NetworkUtils.getCorrelationId(response: response)
        if(lastAuthCorrelationId != nil && identityFromServer != ""){
            if(lastAuthCorrelationId == identityFromServer && NetworkUtils.checkIfSuccess(response: response)){
                setIsAlreadyAuthenticated(value: true)
            }
        }
    }
}

extension SocketManager: WebSocketDelegate {
    
    func connectSocket(){
        if(!isAlreadyRequestedForConnect){
            isAlreadyRequestedForConnect = true
            webSocket?.connect()
        }
    }
    
    func clearIdentityCache(){
        setIsAlreadyAuthenticated(value: false)
    }
    
    func didReceive(event: WebSocketEvent, client: WebSocketClient) {
        print(event)
        print(client)
        switch event {
        case .text(let text):
            print("Received text: \(text)")
            self.checkForSocketResponse(text: text)
        case .connected(let params):
            print("Socket connected with params: \(params)")
            self.socketConnected(event: event)
        case .disconnected( _, _):
            self.socketDisconnected(event: event)
        case .error(let error):
            print("Error in socket connection \(error?.localizedDescription ?? "")")
            isAlreadyRequestedForConnect = false
            setIsAlreadyAuthenticated(value: false)
        case .cancelled:
            self.socketDisconnected(event: event)
        default:
            break
        }
    }
    
    private func socketConnected(event: WebSocketEvent){
        self.stopAutoConnectJob()
        isAlreadyRequestedForConnect = false
        currentStatus = event
        self.delegate?.onWsConnected()
    }
    
    private func socketDisconnected(event: WebSocketEvent){
        currentStatus = event
        isAlreadyRequestedForConnect = false
        setIsAlreadyAuthenticated(value: false)
        self.delegate?.onWsDisconnected()
    }
    
    private func checkForSocketResponse(text:String){
        let data = text.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            {
                print(jsonArray) // use the json here
                if let corId = NetworkUtils.getCorrelationId(response: jsonArray), corId != ""{
                    let completion = self.resultCompletion[corId]
                    //removing core id from response hashmap
                    self.resultCompletion.removeValue(forKey: corId)
                    completion?(corId, .success(jsonArray),false)
                    updateSocketAuthenticationStatus(response: jsonArray)
                }else if NetworkUtils.hasServerMessage(response: jsonArray){
                    self.handleServerMessage(response: jsonArray)
                }
            } else {
                print("json not valid")
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    private func handleServerMessage(response:Dictionary<String, Any>){
        self.delegate?.onServerMessage(msj: response)
    }
}

extension SocketManager {
    
    func reConnectSocket(){
        switch self.currentStatus {
        case .disconnected(_, _):
            self.connectSocket()
        case .cancelled:
            self.connectSocket()
        default:
            break
        }
    }
    
    func disconnectSocket(){
        webSocket?.disconnect()
    }
    
    @objc
    open func appInBackground(){
        timestamp = self.getCurrentMillis()
        if(!useWebsocketInBackground){
            //Starting a job which will disconnect the socket after an interval
            self.startAutoCloseJob()
        }
    }
    
    @objc
    open func appInForeground(){
        timestamp = self.getCurrentMillis()
        stopAutoCloseJob()
        self.checkForSocketReConnect()
    }
    
    @objc
    private func scheduledJobTriggered(){
        if(self.getCurrentMillis() - timestamp > SOCKET_AUTO_BACKGROUND_CLOSE_TIME){
            self.disconnectSocket()
        }
    }
    
    private func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    private func startAutoCloseJob(){
        // close if already exist
        if(autoCloseJob != nil){
            autoCloseJob?.invalidate()
        }

        autoCloseJob = Timer.scheduledTimer(timeInterval: Double(SOCKET_AUTO_BACKGROUND_CLOSE_TIME/1000), target: self, selector: #selector(scheduledJobTriggered), userInfo: nil, repeats: false)
    }
    
    private func stopAutoCloseJob(){
        guard let job = autoCloseJob else {
            return
        }
        autoCloseJob == nil
        if(job.isValid){
            job.invalidate()
        }
    }
    
    @objc private func checkForSocketReConnect(){
        self.stopAutoConnectJob()
        if autoCloseJob == nil || !autoCloseJob!.isValid {
            if !isConnected {
                isAlreadyRequestedForConnect = false
                self.connectSocket()
                autoConnectJob = Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(checkForSocketReConnect), userInfo: nil, repeats: false)
            }
        }
    }
    
    @objc
    private func scheduledReConnectSocket(){
        self.connectSocket()
    }
    
    private func stopAutoConnectJob(){
        guard let job = autoConnectJob else {
            return
        }
        if(job.isValid){
            job.invalidate()
        }
    }
    
    @objc func showOfflineDeviceUI(notification: Notification) {
        if NetworkMonitor.shared.isConnected {
            self.connectSocket()
        } else {
            self.disconnectSocket()
        }
    }
    
    @objc func socketRequestTimeOutCall(_ timer:Timer) {
        //if response not found transfer request to http and remove from resultcompletion
        if let corId = timer.userInfo as? String,let completion = self.resultCompletion[corId] {
            //removing core id from response hashmap
            self.resultCompletion.removeValue(forKey: corId)
            print("Socket Request time out")
            completion(corId, .failure(NSError(domain: "Request timeout", code: -1001, userInfo: nil)),true)
        }
    }

}

