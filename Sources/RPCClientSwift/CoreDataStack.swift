//
//  File.swift
//  
//
//  Created by Muhammad Usman on 13/03/2024.
//

import Foundation
import CoreData

open class PersistentContainer: NSPersistentContainer {
}

internal class CoreDataStack {
    static let shared = CoreDataStack()
    
    private init() {
    }
    
    lazy public var persistentContainer: PersistentContainer? = {
        guard let modelURL = Bundle.module.url(forResource:"RPCClientSwift", withExtension: "momd") else { return nil }
        guard let model = NSManagedObjectModel(contentsOf: modelURL) else { return nil }
        let container = PersistentContainer(name:"RPCClientSwift",managedObjectModel:model)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    var viewContext: NSManagedObjectContext? {
        return persistentContainer?.viewContext
    }
    
    func saveContext() {
        if let context = viewContext, context.hasChanges {
            do {
                try viewContext?.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func saveDataToCoreData(data: [String: Any], apiKey:String){
        guard let context = CoreDataStack.shared.viewContext else {return}
        let entity = NSEntityDescription.entity(forEntityName: "CachedData", in: context)!
        let cachedData = NSManagedObject(entity: entity, insertInto: context)
        
        do {
            if let data = self.fetchDataFromCoreData(apiKey: apiKey) {
                self.removeCacheData(apiKey: apiKey)
            }
            cachedData.setValue(data as AnyObject, forKey: "requestResponse")
            cachedData.setValue(apiKey, forKey: "apiKey")
            try context.save()
            print("Date saved in CoreData")
        } catch {
            print("Error saving data to Core Data: \(error)")
        }
    }
    
    func fetchDataFromCoreData(apiKey:String) -> [String: Any]? {
        let context = CoreDataStack.shared.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CachedData")
        fetchRequest.predicate = NSPredicate(format: "apiKey == %@", apiKey)
        do {
            let cachedData = try context?.fetch(fetchRequest).last
            if let jsonString = cachedData?.value(forKey: "requestResponse") as? AnyObject {
                if let data = jsonString as? [String:Any] {
                    return data
                }
            }
        } catch {
            print("Error fetching data from Core Data: \(error)")
        }
        
        return nil
    }
    
    func removeCacheData(apiKey:String? = nil) {
        let context = CoreDataStack.shared.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CachedData")
        if let apiKey = apiKey {
            fetchRequest.predicate = NSPredicate(format: "apiKey == %@", apiKey)
        }
        do {
            if let cachedData = try context?.fetch(fetchRequest) {
                for data in cachedData {
                    context?.delete(data)
                }
                try context?.save()
            }
        } catch {
            print("Error fetching data from Core Data: \(error)")
        }
    }
    
    func fetchLastClearedDate() -> Date? {
        let context = CoreDataStack.shared.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CacheTimeStamp")
        do {
            let cachedData = try context?.fetch(fetchRequest).last
            if let date = cachedData?.value(forKey: "lastClearedCache") as? Date {
                return date
            }
        } catch {
            print("Error fetching data from Core Data: \(error)")
        }
        
        return nil
    }
    
    
    func updateLastClearedDate(date: Date){
        guard let context = CoreDataStack.shared.viewContext else {return}
        let entity = NSEntityDescription.entity(forEntityName: "CacheTimeStamp", in: context)!
        let cachedData = NSManagedObject(entity: entity, insertInto: context)
        do {
            cachedData.setValue(date, forKey: "lastClearedCache")
            try context.save()
            print("Date saved in CoreData")
        } catch {
            print("Error saving data to Core Data: \(error)")
        }
    }
    
    
}
