//
//  File.swift
//  
//
//  Created by Muhammad Usman on 28/06/2022.
//

import Foundation

internal class ClientMessageRequest {
    
    var payload: Any
    var identity: Identity?
    
    init(payload: Any, identity: Identity?) {
        self.payload = payload
        self.identity = identity
    }
    
    func createBody() throws -> Data {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: getDictionary(), options: [.prettyPrinted])
            return jsonData
        } catch {
            throw error
        }
    }
    
    func createBody(excludeIdentity:Bool) throws -> Data {
        do {
            var params = getDictionary()
            if(excludeIdentity && hasIdentity()){
                params.removeValue(forKey: ParameterKey.identity)
            }
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: [.prettyPrinted])
            return jsonData
        } catch {
            throw error
        }
    }
    
    func hasIdentity()->Bool{
        let params = getDictionary()
        let keyExists = params[ParameterKey.identity] != nil
        return keyExists
    }
    
    
    private func getDictionary() -> StringAny {
        var allParams = StringAny()
        allParams[ParameterKey.client_message] = payload
        if let identity = identity {
            allParams[ParameterKey.identity] = identity.getDictionary()
        }
        return allParams
    }
    
}
