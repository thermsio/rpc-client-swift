import Foundation

typealias ParameterKey = NetworkingSetup.APIParameterKey
typealias HTTPHeaderField = NetworkingSetup.HTTPHeaderField
typealias ContentType = NetworkingSetup.ContentType

internal struct NetworkingSetup {
    
    struct APIParameterKey {
        static let args = "args"
        static let procedure = "procedure"
        static let scope = "scope"
        static let version = "version"
        static let authorization = "authorization"
        static let deviceName = "device_name"
        static let metadata = "metadata"
        static let identity = "identity"
        static let correlationID = "correlationId"
        static let success = "success"
        static let client_message = "clientMessage"
        static let server_message = "serverMessage"
    }
    
    struct HTTPHeaderField {
        static let authorization = "token"
        static let contentType = "Content-Type"
        static let acceptType = "Accept"
        static let timezone = "timezone"
    }
    
    struct ContentType {
        static let json = "application/json"
    }
}
